﻿using AutoMapper;
using Business.Declarations;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Models;
using System.Collections.Generic;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : CustomControllerBase<IClientLogic, Client, long>
    {
        public ClientController(IClientLogic logic, IMapper mapper) : base(logic, mapper)
        {
        }

        [HttpGet("GetClients")]
        public IEnumerable<ClientModel> GetAllClient()
        {
            IEnumerable<Client> clients = Logic.GetAll();

            return Mapper.Map<IEnumerable<ClientModel>>(clients);
        }

        [HttpPost("AddClient")]
        public void InsertClient(ClientModel model)
        {
            var client = Mapper.Map<Client>(model);

            Logic.Save(client);
        }

        [HttpPost("EditClient")]
        public void UpdateClient(ClientModel model)
        {
            var client = Mapper.Map<Client>(model);

            ThrowExceptionIfIssueIsInvalid(client, "El cliente que se quiere actualizar no es valido.");

            Logic.Update(client);
        }
    }
}
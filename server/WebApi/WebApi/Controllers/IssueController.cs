﻿using AutoMapper;
using Business.Declarations;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Models;
using System.Collections.Generic;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IssueController : CustomControllerBase<IIssueLogic, Issue, long>
    {
        public IssueController(IIssueLogic logic, IMapper mapper) : base(logic, mapper)
        {
        }

        [HttpGet("GetIssuesOfClient")]
        public IEnumerable<IssueModel> GetAllIssueByClientId(int clientId)
        {
            IEnumerable<Issue> issues = Logic.GetAllIssueByClientId(clientId);

            return Mapper.Map<IEnumerable<IssueModel>>(issues);
        }

        [HttpPost("AddIssue")]
        public void InsertIssue(IssueModel model)
        {
            var client = Mapper.Map<Issue>(model);

            Logic.Save(client);
        }

        [HttpPost("EditIssue")]
        public void UpdateIssue(IssueModel model)
        {
            var issueToUpdate = Mapper.Map<Issue>(model);

            ThrowExceptionIfIssueIsInvalid(issueToUpdate, "El problema que se quiere actualizar no es valido.");

            Logic.Update(issueToUpdate);
        }
    }
}
﻿using AutoMapper;
using Business.Declarations;
using Commons.Exceptions;
using Domain;
using Microsoft.AspNetCore.Mvc;
using System;

namespace WebApi.Controllers
{
    public abstract class CustomControllerBase<TLogic, TEntity, TID> : ControllerBase
        where TLogic : ILogicBase<TEntity, TID>
        where TEntity : EntityBase<TID>, IEquatable<TEntity>
        where TID : struct
    {
        protected TLogic Logic { get; }
        protected IMapper Mapper { get; }

        public CustomControllerBase(TLogic logic, IMapper mapper)
        {
            Logic = logic;
            Mapper = mapper;
        }

        protected void ThrowExceptionIfIssueIsInvalid(TEntity entityToControl, string exceptionMenssage)
        {
            TEntity entityFromDB = Logic.GetById(entityToControl.Id);

            if (entityFromDB == null || !entityFromDB.Equals(entityToControl))
            {
                throw new BusinessException(exceptionMenssage);
            }
        }
    }
}
﻿using AutoMapper;
using Domain;
using Models;

namespace WebApi.AutoMapper
{
    public class SettingsProfile : Profile
    {
        public SettingsProfile()
        {
            CreateMap<Client, ClientModel>();

            CreateMap<ClientModel, Client>();

            CreateMap<Issue, IssueModel>();

            CreateMap<IssueModel, Issue>();
        }
    }
}
﻿using Business.Declarations;
using Business.Implementations;
using Constants.StringKeys;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Repositories.Declarations;
using Repositories.Implementations;

namespace Infrastructure
{
    internal static class DependencyInjections
    {
        public static void InitializeBusinessLogicsInjections(this IServiceCollection service)
        {
            service.AddSingleton<IClientLogic, ClientLogic>();
            service.AddSingleton<IIssueLogic, IssueLogic>();
        }

        public static void InitializeRepositoriesInjections(this IServiceCollection service)
        {
            service.AddSingleton<IClientRepository, ClientRepository>();
            service.AddSingleton<IIssueRepository, IssueRepository>();
        }
    }
}
﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure
{
    public static class Handler
    {
        public static IApplicationBuilder UseHttpStatusCodeExceptionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CustomMiddleware>();
        }

        public static void AddDependencyInjections(this IServiceCollection service)
        {
            service.InitializeBusinessLogicsInjections();
            service.InitializeRepositoriesInjections();
        }
    }
}
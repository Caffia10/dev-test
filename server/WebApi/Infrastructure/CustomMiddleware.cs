﻿using Commons.Exceptions;
using Constants.StringKeys;
using DataLayer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class CustomMiddleware
    {
        private readonly RequestDelegate next;
        private readonly ILogger<CustomMiddleware> logger;

        public CustomMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            this.next = next ?? throw new ArgumentNullException(nameof(next));
            logger = loggerFactory?.CreateLogger<CustomMiddleware>() ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                BeginRequest(context);
                await next(context);
                EndRequest(context);
            }
            catch (BusinessException ex)
            {
                if (context.Response.HasStarted)
                {
                    logger.LogWarning("The response has already started, the http status code middleware will not be executed.");
                    throw;
                }

                context.Response.Clear();
                context.Response.StatusCode = ex.StatusCode;
                context.Response.ContentType = ex.ContentType;

                await context.Response.WriteAsync(ex.Message);
            }
        }

        private void BeginRequest(HttpContext context)
        {
            InitializeSession(context);
        }

        private void EndRequest(HttpContext context)
        {
            CloseSession(context);
        }

        private void InitializeSession(HttpContext context)
        {
            SetCurrentSession(context, UnitOfWork.OpenSessionAndBeginTransaction());
        }

        private void CloseSession(HttpContext context)
        {
            UnitOfWork.CloseSession(GetCurrentSession(context));
        }

        private object GetCurrentSession(HttpContext context)
        {
            return context.Items[HttpContextKeys.HTTPCONTEXT_SESSION];
        }

        private void SetCurrentSession(HttpContext context, object value)
        {
            context.Items[HttpContextKeys.HTTPCONTEXT_SESSION] = value;
        }
    }
}
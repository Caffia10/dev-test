﻿using Newtonsoft.Json.Linq;
using System;
using System.Net;

namespace Commons.Exceptions
{
    public class BusinessException : Exception
    {
        public int StatusCode { get; set; }
        public string ContentType { get; set; } = @"text/plain";

        public BusinessException(string message) : base(message)
        {
            StatusCode = (int)HttpStatusCode.Conflict;
        }

        public BusinessException(int statusCode)
        {
            StatusCode = statusCode;
        }

        public BusinessException(int statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        public BusinessException(int statusCode, Exception inner) : this(statusCode, inner.ToString())
        {
        }

        public BusinessException(int statusCode, JObject errorObject) : this(statusCode, errorObject.ToString())
        {
            ContentType = @"application/json";
        }
    }
}
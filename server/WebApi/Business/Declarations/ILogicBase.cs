﻿using Domain;
using System.Collections.Generic;

namespace Business.Declarations
{
    public interface ILogicBase<TEntity, TID>
        where TEntity : EntityBase<TID>
        where TID : struct
    {
        void Save(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);

        TEntity GetById(TID id);

        IList<TEntity> GetAll();
    }
}
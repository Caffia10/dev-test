﻿using Domain;
using System.Collections.Generic;

namespace Business.Declarations
{
    public interface IIssueLogic : ILogicBase<Issue, long>
    {
        IEnumerable<Issue> GetAllIssueByClientId(int clientId);
    }
}
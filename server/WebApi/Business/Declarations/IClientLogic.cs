﻿using Domain;

namespace Business.Declarations
{
    public interface IClientLogic : ILogicBase<Client, long>
    {
    }
}
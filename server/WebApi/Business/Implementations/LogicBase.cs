﻿using Business.Declarations;
using Domain;
using Repositories.Declarations;
using System.Collections.Generic;

namespace Business.Implementations
{
    public abstract class LogicBase<TEntity, TID, TRepository> : ILogicBase<TEntity, TID>
        where TEntity : EntityBase<TID>
        where TID : struct
        where TRepository : IRepositoryBase<TEntity, TID>
    {
        protected TRepository Repository { get; }

        public LogicBase(TRepository repository)
        {
            Repository = repository;
        }

        public virtual void Delete(TEntity entity) => Repository.Delete(entity);

        public virtual IList<TEntity> GetAll() => Repository.GetAll();

        public virtual TEntity GetById(TID id) => Repository.GetById(id);

        public virtual void Save(TEntity entity) => Repository.Save(entity);

        public virtual void Update(TEntity entity) => Repository.Update(entity);
    }
}
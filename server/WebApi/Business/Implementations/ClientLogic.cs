﻿using Business.Declarations;
using Domain;
using Repositories.Declarations;

namespace Business.Implementations
{
    public class ClientLogic : LogicBase<Client, long, IClientRepository>, IClientLogic
    {
        public ClientLogic(IClientRepository repository) : base(repository)
        {
        }
    }
}
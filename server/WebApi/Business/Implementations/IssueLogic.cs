﻿using Business.Declarations;
using Domain;
using Repositories.Declarations;
using System.Collections.Generic;

namespace Business.Implementations
{
    public class IssueLogic : LogicBase<Issue, long, IIssueRepository>, IIssueLogic
    {
        public IssueLogic(IIssueRepository repository) : base(repository)
        {
        }

        public IEnumerable<Issue> GetAllIssueByClientId(int clientId) => Repository.GetAllIssueByClientId(clientId);
    }
}
﻿using Constants.Configs;
using Constants.Enums;
using DataMaps;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System;
using ISession = NHibernate.ISession;

namespace DataLayer
{
    public static class UnitOfWork
    {
        private static ISessionFactory sessionFactory;

        private static ISessionFactory SessionFactory
        {
            get
            {
                if (sessionFactory == null)
                {
                    sessionFactory = Fluently.Configure()
                                             .Database(GetDatabase())
                                             .Mappings(m => m.FluentMappings.AddFromAssemblyOf<ClientMap>())
                                             // Todo : esto lo dejamo pero sirve para crear todoa la db desde 0 sin necesidad de ejecutar scripts manualmente.
                                             //.ExposeConfiguration(cfg => new NHibernate.Tool.hbm2ddl.SchemaExport(cfg).Create(true, true))
                                             .BuildConfiguration()
                                             .BuildSessionFactory();
                }
                return sessionFactory;
            }
        }

        private static IPersistenceConfigurer GetDatabase()
        {
            return EnverimontConfigs.Instance.DatabaseManagementSystemType switch
            {
                DatabaseManagementSystemType.SQLServer => MsSqlConfiguration.MsSql2012.ConnectionString(EnverimontConfigs.Instance.ConnectionString),

                DatabaseManagementSystemType.PostgreSQL => PostgreSQLConfiguration.Standard.ConnectionString(EnverimontConfigs.Instance.ConnectionString),

                DatabaseManagementSystemType.Oracle => OracleClientConfiguration.Oracle10.ConnectionString(EnverimontConfigs.Instance.ConnectionString),

                _ => throw new InvalidOperationException("Error base de datos no configurada"),
            };
        }

        public static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }

        public static IStatelessSession OpenStatelessSession()
        {
            return SessionFactory.OpenStatelessSession();
        }

        public static void BeginTransaction(ISession currentSession)
        {
            currentSession.BeginTransaction();
        }

        public static ISession OpenSessionAndBeginTransaction()
        {
            ISession session = OpenSession();
            session.BeginTransaction();

            return session;
        }

        public static void CloseSession(object currentSession) => CloseSession(currentSession as ISession);

        public static void CloseSession(ISession currentSession)
        {
            if (currentSession != null)
            {
                if (currentSession.Transaction != null && currentSession.Transaction.IsActive)
                {
                    currentSession.Transaction.Commit();
                }
                currentSession.Clear();
                currentSession.Dispose();
            }
        }
    }
}
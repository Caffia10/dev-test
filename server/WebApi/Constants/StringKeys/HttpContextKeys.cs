﻿namespace Constants.StringKeys
{
    public static class HttpContextKeys
    {
        public const string HTTPCONTEXT_SESSION = "current.session";
    }
}
﻿namespace Constants.Enums
{
    public enum DatabaseManagementSystemType
    {
        SQLServer = 0,
        PostgreSQL = 1,
        Oracle = 2
    }
}
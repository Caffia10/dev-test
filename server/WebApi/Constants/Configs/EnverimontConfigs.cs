﻿using Constants.Enums;
using System;

namespace Constants.Configs
{
    public class EnverimontConfigs
    {
        private static EnverimontConfigs instance;

        public static EnverimontConfigs Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EnverimontConfigs();
                }

                return instance;
            }
        }

        private EnverimontConfigs()
        {
        }

        public string ConnectionString { get; set; }

        private int codeTypeOfDatabaseManagementSystem;

        public string CodeTypeOfDatabaseManagementSystem
        {
            get => codeTypeOfDatabaseManagementSystem.ToString();
            set
            {
                codeTypeOfDatabaseManagementSystem = Convert.ToInt16(value);
                OnChangeCodeTypeOfDatabaseManagementSystem(codeTypeOfDatabaseManagementSystem);
            }
        }

        public DatabaseManagementSystemType DatabaseManagementSystemType { get; private set; }

        private void OnChangeCodeTypeOfDatabaseManagementSystem(int newCode)
        {
            DatabaseManagementSystemType = (DatabaseManagementSystemType)newCode;
        }
    }
}
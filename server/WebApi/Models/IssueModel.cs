﻿namespace Models
{
    public class IssueModel : ModelBase<long>
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsResolved { get; set; }

        public string Resolution { get; set; }

        public ClientModel Client { get; set; }
    }
}
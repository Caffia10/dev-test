﻿namespace Models
{
    public class ClientModel : ModelBase<long>
    {
        public string FullName { get; set; }

        public string Document { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public int NumberOfIssueToResolve { get; set; }

        public int NumberOfIssueResolved { get; set; }
    }
}
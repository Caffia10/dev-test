﻿namespace Models
{
    public abstract class ModelBase<TID>
    {
        public TID Id { get; set; }

        public bool IsRecordDisabled { get; set; }

        public int Version { get; set; }
    }
}
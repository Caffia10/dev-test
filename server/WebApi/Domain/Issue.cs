﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Domain
{
    public class Issue : EntityBase<long>, IEquatable<Issue>
    {
        public virtual string Name { get; set; }

        public virtual string Description { get; set; }

        public virtual bool IsResolved { get; set; }

        public virtual string Resolution { get; set; }

        public virtual Client Client { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Issue issue ? Equals(issue) : false;
        }

        public virtual bool Equals([AllowNull] Issue other)
        {
            var isEqual = false;

            if (other != null)
            {
                isEqual = other.Id == Id && other.Client.Equals(Client);
            }

            return isEqual;
        }

        public override int GetHashCode()
        {
            return Id == 0 ? base.GetHashCode() : Id.GetHashCode();
        }
    }
}
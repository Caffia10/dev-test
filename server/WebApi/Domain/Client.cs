﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Domain
{
    public class Client : EntityBase<long>, IEquatable<Client>
    {
        public virtual string FullName { get; set; }

        public virtual string Document { get; set; }

        public virtual string Address { get; set; }

        public virtual string Phone { get; set; }

        public virtual int NumberOfIssueToResolve { get; set; }

        public virtual int NumberOfIssueResolved { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Client client ? Equals(client) : false;
        }

        public virtual bool Equals([AllowNull] Client other)
        {
            var isEqual = false;

            if (other != null)
            {
                isEqual = other.Id == Id;
            }

            return isEqual;
        }

        public override int GetHashCode()
        {
            return Id == 0 ? base.GetHashCode() : Id.GetHashCode();
        }
    }
}
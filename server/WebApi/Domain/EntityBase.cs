﻿namespace Domain
{
    public abstract class EntityBase<TID>
    {
        public virtual TID Id { get; set; }

        public virtual bool IsRecordDisabled { get; set; }

        public virtual int Version { get; set; }
    }
}
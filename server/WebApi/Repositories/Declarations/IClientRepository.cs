﻿using Domain;

namespace Repositories.Declarations
{
    public interface IClientRepository : IRepositoryBase<Client, long>
    {
    }
}
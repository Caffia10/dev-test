﻿using Domain;
using System.Collections.Generic;

namespace Repositories.Declarations
{
    public interface IRepositoryBase<TEntity, TID>
         where TEntity : EntityBase<TID>
         where TID : struct
    {
        void Save(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);

        TEntity GetById(TID id);

        IList<TEntity> GetAll();

        TEntity GetLoadedEntityById(TID id);

        void Evict(TEntity entity);
    }
}
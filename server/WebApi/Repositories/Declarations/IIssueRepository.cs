﻿using Domain;
using System.Collections.Generic;

namespace Repositories.Declarations
{
    public interface IIssueRepository : IRepositoryBase<Issue, long>
    {
        IEnumerable<Issue> GetAllIssueByClientId(int clientId);
    }
}
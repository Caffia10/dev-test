﻿using Domain;
using Microsoft.AspNetCore.Http;
using NHibernate;
using Repositories.Declarations;

namespace Repositories.Implementations
{
    public class ClientRepository : RepositoryBase<Client, long>, IClientRepository
    {
        public ClientRepository(IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
        }
    }
}
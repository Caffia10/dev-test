﻿using Constants.StringKeys;
using Domain;
using Microsoft.AspNetCore.Http;
using NHibernate;
using Repositories.Declarations;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using ISession = NHibernate.ISession;

namespace Repositories.Implementations
{
    public abstract class RepositoryBase<TEntity, TID> : IRepositoryBase<TEntity, TID>
        where TEntity : EntityBase<TID>
        where TID : struct
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        protected ISession Session => httpContextAccessor.HttpContext.Items[HttpContextKeys.HTTPCONTEXT_SESSION] as ISession;

        public RepositoryBase(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public void Delete(TEntity entity)
        {
            Session.Delete(entity);
        }

        public TEntity GetById(TID id)
        {
            return Session.Get<TEntity>(id);
        }

        public void Save(TEntity entity)
        {
            Session.Save(entity);
        }

        public void Update(TEntity entity)
        {
            TEntity entityLoaded = GetLoadedEntityById(entity.Id);

            if (entityLoaded.Equals(entity))
            {
                Evict(entityLoaded);
            }

            Session.Update(entity);
        }

        public IList<TEntity> GetAll()
        {
            return Session.QueryOver<TEntity>().List();
        }

        protected IQueryOver<TEntity, TEntity> GetQueryOverByCondition(Expression<Func<TEntity, bool>> expression)
        {
            return Session.QueryOver<TEntity>().Where(expression);
        }

        public TEntity GetLoadedEntityById(TID id)
        {
            return Session.Load<TEntity>(id);
        }

        public void Evict(TEntity entity)
        {
            Session.Evict(entity);
        }
    }
}
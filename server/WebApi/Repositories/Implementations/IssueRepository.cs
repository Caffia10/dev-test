﻿using Domain;
using Microsoft.AspNetCore.Http;
using NHibernate;
using Repositories.Declarations;
using System.Collections.Generic;

namespace Repositories.Implementations
{
    public class IssueRepository : RepositoryBase<Issue, long>, IIssueRepository
    {
        public IssueRepository(IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
        }

        public IEnumerable<Issue> GetAllIssueByClientId(int clientId)
        {
            return GetQueryOverByCondition(x => x.Client.Id == clientId).List();
        }
    }
}
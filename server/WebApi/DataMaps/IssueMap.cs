﻿using Domain;

namespace DataMaps
{
    public class IssueMap : EntityBaseMap<Issue, long>
    {
        internal const string TABLE_NAME = "issues";

        internal const string FK_NAME_TO_CLIENT = "client_id";

        internal const string IS_RESOLVED_COLUMN_NAME = "is_resolved";

        public IssueMap() : base(TABLE_NAME)
        {
            Map(x => x.Name).Column("name").Not.Nullable();

            Map(x => x.Description).Column("description").Not.Nullable();

            Map(x => x.IsResolved).Column(IS_RESOLVED_COLUMN_NAME).Not.Nullable();

            Map(x => x.Resolution).Column("resolution").Nullable();

            References(x => x.Client).Column(FK_NAME_TO_CLIENT).LazyLoad();
        }
    }
}
﻿using Domain;

namespace DataMaps
{
    public class ClientMap : EntityBaseMap<Client, long>
    {
        internal const string TABLE_NAME = "clients";

        public ClientMap() : base("clients")
        {
            Map(x => x.FullName).Column("full_name").Not.Nullable();

            Map(x => x.Document).Column("document").Not.Nullable();

            Map(x => x.Address).Column("addres").Nullable();

            Map(x => x.Phone).Column("phone").Nullable();

            Map(x => x.NumberOfIssueResolved)

                .Formula(MapUtils.GetCountFormula(IssueMap.TABLE_NAME,
                                                  IssueMap.FK_NAME_TO_CLIENT,
                                                  MapUtils.GetBooleanConditionForFormula(IssueMap.TABLE_NAME, IssueMap.IS_RESOLVED_COLUMN_NAME, true)));

            Map(x => x.NumberOfIssueToResolve)
                .Formula(MapUtils.GetCountFormula(IssueMap.TABLE_NAME,
                                                  IssueMap.FK_NAME_TO_CLIENT,
                                                  MapUtils.GetBooleanConditionForFormula(IssueMap.TABLE_NAME, IssueMap.IS_RESOLVED_COLUMN_NAME, false)));
        }
    }
}
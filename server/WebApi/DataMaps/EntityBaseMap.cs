﻿using Domain;
using FluentNHibernate.Mapping;

namespace DataMaps
{
    public class EntityBaseMap<T, ID> : ClassMap<T>
        where T : EntityBase<ID>
        where ID : struct
    {
        public EntityBaseMap(string tableName)
        {
            Table(tableName);

            Id(x => x.Id)
                .Column("id")
                .CustomSqlType("Serial")
                .GeneratedBy
                .Native();

            Map(x => x.IsRecordDisabled).Column("is_record_disabled").Not.Nullable();

            Version(x => x.Version);
        }
    }
}
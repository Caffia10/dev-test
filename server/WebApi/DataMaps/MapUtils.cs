﻿using Commons;
using Constants.Configs;
using Constants.Enums;

namespace DataMaps
{
    public static class MapUtils
    {
        public static string GetCountFormula(string tableName, string fk, string extraCondition = null)
        {
            return $"(SELECT COUNT(tAlias_{tableName}.id) FROM {tableName} AS tAlias_{tableName} WHERE tAlias_{tableName}.{fk} = id {(!extraCondition.IsNullOrEmpty() ? $"AND {extraCondition}" : string.Empty)})";
        }

        public static string GetBooleanConditionForFormula(string tableName, string columnName, bool valueToFind)
        {
            if (!columnName.IsNullOrEmpty())
            {
                return EnverimontConfigs.Instance.DatabaseManagementSystemType switch
                {
                    DatabaseManagementSystemType.PostgreSQL => $"tAlias_{tableName}.{columnName} = {(valueToFind ? "true" : "false")}",

                    DatabaseManagementSystemType.SQLServer => $"tAlias_{tableName}.{columnName} = {(valueToFind ? "1" : "0")}",

                    DatabaseManagementSystemType.Oracle => $"tAlias_{tableName}.{columnName} = {(valueToFind ? "TRUE" : "FALSE")}",

                    _ => null
                };
            }
            else
            {
                return null;
            }
        }
    }
}
import BaseModel from '@/models/base-model';

export default class ClientModel extends BaseModel {
  public fullName!: string;

  public document!: string;

  public address?: string;

  public phone?: string;

  public numberOfIssueToResolve?: number;

  public numberOfIssueResolved?: number;
}

import BaseModel from '@/models/base-model';
import ClientModel from '@/models/client-model';

export default class IssueModel extends BaseModel {
  public name!: string;

  public description!: string;

  public isResolved!: boolean;

  public resolution?: string;

  public client!: ClientModel;
}

export default abstract class BaseModel {
  public id!: number;
  public isRecordDisabled!: boolean;
  public version!: number;
}

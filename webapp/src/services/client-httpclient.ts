import WebHTTPClientBase from '@/services/web-httpclient-base';
import ClientModel from '@/models/client-model';

export default class ClientHTTPClient extends WebHTTPClientBase<ClientModel> {
  constructor() {
    super('Client', ClientModel);
  }

  public getClients() {
    return this.getCollection('GetClients');
  }

  public addClient(client:ClientModel){
    return this.post("AddClient",client);
  }

  public editClient(client:ClientModel){
    return this.post("EditClient",client);
  }
}

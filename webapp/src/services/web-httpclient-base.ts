import axios, { AxiosResponse } from 'axios';
import {EnverimonConfigHandler} from '@/configs/enverimont-config-handler';

axios.defaults.baseURL = EnverimonConfigHandler.GetInstance().apiURLBase + "/api/";
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/json';
// axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';

interface ModelFactory<T> {
  new (): T;
}

export default abstract class WebHTTPClientBase<TModel> {
  private baseUrl: string;

  private factory: ModelFactory<TModel>;

  constructor(controllerName: string, factory: ModelFactory<TModel>) {
    this.baseUrl = controllerName;
    this.factory = factory;
  }

  protected async getSingleResult(
    getMethodName: string = ''
  ): Promise<TModel | undefined> {
    let response: AxiosResponse<any>;
    try {
      response = await axios.get(this.getFullUrl(getMethodName));

      return Object.assign(new this.factory(), response.data);
    } catch (error) {
      // TODO : deberiamos ver como transmitimos los errores al cliente, con algun tipo de popup seria lo ideal.
    }
  }

  protected async getCollection(
    getMethodName: string,
    params?: any
  ): Promise<TModel[] | undefined> {
    let arrayResult: TModel[];
    let response: AxiosResponse<any>;
    try {
      arrayResult = [];




      response = await axios.get(this.getFullUrl(getMethodName),{params:params});
      response.data.forEach((element: any) => {
        arrayResult.push(Object.assign(new this.factory(), element));
      });

      return arrayResult;
    } catch (error) {
      // TODO : deberiamos ver como transmitimos los errores al cliente, con algun tipo de popup seria lo ideal.
    }
  }

  protected async post(postMethodName: string, data?:any) {

    try {
      await axios.post(this.getFullUrl(postMethodName), data);

    } catch (error) {
      // TODO : deberiamos ver como transmitimos los errores al cliente, con algun tipo de popup seria lo ideal.
    }

  }

  private getFullUrl(methodName: string) {


    return `${this.baseUrl}/${methodName}`;
  }
}

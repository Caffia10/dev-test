import WebHTTPClientBase from '@/services/web-httpclient-base';
import IssueModel from '@/models/issue-model';

export default class IssueHTTPClient extends WebHTTPClientBase<IssueModel> {
  constructor() {
    super('Issue', IssueModel);
  }

  public getIssuesByClient(clientId:any) {
    return this.getCollection('GetIssuesOfClient', {clientId:clientId});
  }

  public addIssue(issue:IssueModel){
    return this.post("AddIssue",issue);
  }

  public editIssue(issue:IssueModel){
    return this.post("EditIssue",issue);
  }

}

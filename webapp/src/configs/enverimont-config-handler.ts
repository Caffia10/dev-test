
const config = require(`@/configs/enverimontconfig.${(process.env.NODE_ENV === 'production' ? 'prod' : 'dev')}.json`);

export class EnverimonConfigHandler {
  public apiURLBase!: string;

  private static instance: EnverimonConfigHandler;

  private constructor() {}

  public static GetInstance(): EnverimonConfigHandler{

    if (!this.instance){
      this.instance = this.createInstance();

    }

    return this.instance;
  }


  private static createInstance():  EnverimonConfigHandler{
    return Object.assign(new EnverimonConfigHandler(), config);
  }
}

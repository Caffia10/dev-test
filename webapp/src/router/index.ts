import Vue from 'vue';
import VueRouter from 'vue-router';
import ClientList from '../views/clients/ClientList.vue';
import ClientManagement from '../views/clients/ClientManagement.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'clientsList',
    component: ClientList,
  },
  {
    path: '/clientManagement',
    name: 'ClientManagement',
    component: ClientManagement,
    props: true
  }
];

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes,
});

export default router;
